<?php
/**
 * @file cardonfile_expiration.rules.inc
 * TODO: Enter file description here.
 */

/**
 * Implements hook_rules_event_info().
 */
function cardonfile_expiration_rules_event_info() {
  $events = array();
  $events['cardonfile_expiration_card_expiring'] = array(
    'label' => t('Users card expiration'),
    'group' => t('Card on File'),
    'variables' => array(
      'user' => array('type' => 'user', 'label' => t('User')),
      'expires' => array('type' => 'integer', 'label' => t('Expires in X days')),
    ),
  );
  return $events;
}
